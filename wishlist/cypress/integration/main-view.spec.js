describe('main window', () => {
    it('has correct header and english by default', () => {
        cy.visit('http://localhost:4200');
        cy.contains('TW');
        cy.get('h1 b').should('contain', 'HOLA Hi and Welcome');
    });
});