import { Component, OnInit, EventEmitter, Output, IterableDiffers } from '@angular/core';
import { TravelDestination } from './../../models/travel-destination.model';
import { DestinationsApiClient } from './../../models/destinations-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';

// declare var popover: any;

@Component({
  selector: 'app-destination-list',
  templateUrl: './destination-list.component.html',
  styleUrls: ['./destination-list.component.css'],
  providers: [ DestinationsApiClient ]
})
export class DestinationListComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<TravelDestination>;
  updates:string[];
  all;

  constructor(public destinationsApiClient: DestinationsApiClient, private store: Store<AppState>) {
  	this.onItemAdded = new EventEmitter();
    this.updates = [];

/*     this.store.select(state => state.destinations.favorite).subscribe(data => {
      const d =data;
      if (d != null) {
        this.updates.push("You chose " + d.name);
    }
  }); */

  store.select(state => state.destinations.items).subscribe(items => this.all = items);
}

  ngOnInit(): void {
    // new popover();

    this.store.select(state => state.destinations).subscribe(data => {
      let d =data.favorite;
      if (d != null) {
        this.updates.push("You chose " + d.name);
    }
  });
}

  added(d: TravelDestination) {
  	this.destinationsApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  chosen(d: TravelDestination){
    this.destinationsApiClient.chose(d);
  }

  getAll() {}

}
