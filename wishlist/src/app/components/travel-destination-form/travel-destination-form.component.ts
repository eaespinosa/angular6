import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { TravelDestination } from './../../models/travel-destination.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-travel-destination-form',
  templateUrl: './travel-destination-form.component.html',
  styleUrls: ['./travel-destination-form.component.css']
})
export class TravelDestinationFormComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<TravelDestination>;
  fg: FormGroup;
  minLength = 3;
  searchResults!: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
  	this.onItemAdded = new EventEmitter();

  	this.fg = fb.group({
  		name: ['', Validators.compose([
        Validators.required,
        this.nameValidator,
        this.nameValidatorParam(this.minLength)
      ])],
  		url: ['']
  	});

  	this.fg.valueChanges.subscribe((form: any) => {
  		console.log("form modified: ", form);
  	});

    this.fg.controls['name'].valueChanges.subscribe(
    (value: string) => {
      console.log('name modified:', value);
    });
    this.fg.controls['url'].valueChanges.subscribe(
      (value: string) => {
        console.log('url added:', value);
      });
  }

  ngOnInit(): void  {
    let elemName = <HTMLInputElement>document.getElementById('name');
    fromEvent(elemName, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(120),
        distinctUntilChanged(),
        switchMap((text: string) => ajax(this.config.apiEndpoint + '/cities?q=' + text))
        ).subscribe(ajaxResponse => {
          this.searchResults = ajaxResponse.response
        });
      let elemUrl =<HTMLInputElement>document.getElementById('url');
      fromEvent(elemUrl, 'input')
  }

  save(name: string, url: string): boolean {
  	const d = new TravelDestination(name, url);
  	this.onItemAdded.emit(d);
  	return false;
  }

    nameValidator(control: FormControl): { [s: string]: boolean } {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < 3) {
        return { invalidName: true };
      }
      return null;
    }

    nameValidatorParam(minLength: number): ValidatorFn {
      return (control: FormControl) : { [key: string]: boolean } | null => {
        const l = control.value.toString().trim().length;
        if (l > 0 && l < minLength) {
          return { minNameLength: true };
      }
        return null;
    };
  }

}
