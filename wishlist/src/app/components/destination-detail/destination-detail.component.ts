import { Component, Inject, Injectable, InjectionToken, ModuleWithProviders, NgModule, OnInit } from '@angular/core';
import { DestinationsApiClient } from './../../models/destinations-api-client.model';
import { TravelDestination } from './../../models/travel-destination.model';
import { ActivatedRoute } from '@angular/router';
import { AppState } from 'src/app/app.module';

declare const popover: any;

/* class DestinationsApiClientOld {
  getById(id: String): TravelDestination {
    console.log('Calling old class');
    return null;
  }
}

interface AppConfig {
  apiEndpoint: String;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'my_api.com'
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

@Injectable()
class DestinationsApiClientDecorated extends DestinationsApiClient {
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
    super(store);
  }
  getById(id: String): TravelDestination {
    console.log('calling decorated class!');
    console.log('config: ' + this.config.apiEndpoint);
    return super.getById(id);
  }
} */

@Component({
  selector: 'app-destination-detail',
  templateUrl: './destination-detail.component.html',
  styleUrls: ['./destination-detail.component.css'],
  providers: [ DestinationsApiClient
/*     { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    { provide: DestinationsApiClient, useClass: DestinationsApiClientDecorated },
    { provide: DestinationsApiClientOld, useExisting: DestinationsApiClient } */
  ],
})

export class DestinationDetailComponent implements OnInit {
  destination:TravelDestination;
  /* style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      "id": "countries",
      "type": "fill",
      "source": "world",
      "layout": {},
      "paint": {
        'fill-color': '#6F788A'
      }
    }]
  }; */

  constructor(private route: ActivatedRoute, private destinationsApiClient:DestinationsApiClient) { }

  ngOnInit(): void {
    new popover();
  	const id = this.route.snapshot.paramMap.get('id');
  	this.destination = this.destinationsApiClient.getById(id);
  }

}
