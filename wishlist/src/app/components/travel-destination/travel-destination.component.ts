import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteDownAction, VoteUpAction, ResetAction } from '../../models/travel-destinations-state.model';
import { TravelDestination } from './../../models/travel-destination.model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-travel-destination',
  templateUrl: './travel-destination.component.html',
  styleUrls: ['./travel-destination.component.css'],
  animations: [
    trigger('isFavorite', [
      state('favoriteState', style({
        border: '2px solid var(--black)'
      })),
      state('notFavoriteState', style({
        border: 'WhiteSmoke'
      })),
      transition('notFavoriteState => favoriteState', [
        animate('3s')
      ]),
      transition('favoriteState => favoriteState', [
        animate('1s')
      ]),
    ])
  ]
})
export class TravelDestinationComponent implements OnInit {

  @Input() destination!: TravelDestination;
  @Input('idx') position!: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() onClicked: EventEmitter<TravelDestination>;


  constructor(private store: Store<AppState>) { 
  	this.onClicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  go() {
  	this.onClicked.emit(this.destination);
  	return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destination));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destination));
    return false;
  }

  reset() {
    this.store.dispatch(new ResetAction(this.destination));
    return false;
  }

}
