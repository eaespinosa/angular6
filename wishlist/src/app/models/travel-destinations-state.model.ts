import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { TravelDestination } from './travel-destination.model';
import { HttpClientModule } from '@angular/common/http';

// STATE
export interface TravelDestinationsState {
    items: TravelDestination[];
    loading: boolean;
    favorite: TravelDestination;
}

// export const initializeTravelDestinationsState = function() {
export function initializeTravelDestinationsState() {
    return {
        items: [],
        loading: false,
        favorite: null
    };
};

// ACTIONS
export enum TravelDestinationsActionTypes {
    NEW_DESTINATION = '[Travel Destinations] New',
    FAVORITE_CHOSEN = '[Travel Destinations] Favorite',
    VOTE_UP = '[Travel Destinations] Vote Up',
    VOTE_DOWN = '[Travel Destinations] Vote Down',
    RESET = '[Travel Destinations] Reset',
    INIT_MY_DATA = '[Travel Destinations] Init My Data'
}

export class NewDestinationAction implements Action {
    type = TravelDestinationsActionTypes.NEW_DESTINATION;
    constructor(public destination: TravelDestination) {}
}

export class FavoriteChosenAction implements Action {
    type = TravelDestinationsActionTypes.FAVORITE_CHOSEN;
    constructor(public destination: TravelDestination) {}
}

export class VoteUpAction implements Action {
    type = TravelDestinationsActionTypes.VOTE_UP;
    constructor(public destination: TravelDestination) {}
}

export class VoteDownAction implements Action {
    type = TravelDestinationsActionTypes.VOTE_DOWN;
    constructor(public destination: TravelDestination) {}
}

export class ResetAction implements Action {
    type = TravelDestinationsActionTypes.RESET;
    constructor(public destination: TravelDestination) {}
}

export class InitMyDataAction implements Action {
    type = TravelDestinationsActionTypes.INIT_MY_DATA;
    constructor(public destinations: string[]) {}
}

export type TravelDestinationsActions = NewDestinationAction | FavoriteChosenAction | VoteUpAction | VoteDownAction | ResetAction | InitMyDataAction;

export function reducerTravelDestinations(
    state: TravelDestinationsState,
    action: TravelDestinationsActions
): TravelDestinationsState {
    switch (action.type) {
        case TravelDestinationsActionTypes.INIT_MY_DATA: {
            const destinations: string[] = (action as InitMyDataAction).destinations;
            return {
                ...state,
                items: destinations.map((d) => new TravelDestination(d, ''))
            };
        }
        case TravelDestinationsActionTypes.NEW_DESTINATION: {
            return {
                ...state,
                items: [...state.items, (action as NewDestinationAction).destination]
            };
        }
        case TravelDestinationsActionTypes.FAVORITE_CHOSEN: {
            state.items.forEach(x => x.setSelected(false));
            const fav: TravelDestination = (action as FavoriteChosenAction).destination;
            fav.setSelected(true);
            return {
                ...state,
                favorite: fav
            };
        }
        case TravelDestinationsActionTypes.VOTE_UP: {
            const d: TravelDestination = (action as VoteUpAction).destination;
            d.voteUp();
            return {...state};
        }
        case TravelDestinationsActionTypes.VOTE_DOWN: {
            const d: TravelDestination = (action as VoteDownAction).destination;
            d.voteDown();
            return {...state};
        }
        case TravelDestinationsActionTypes.RESET: {
            const d: TravelDestination = (action as ResetAction).destination;
            d.reset();
            return {...state};
        }
    }
    return state;
}

@Injectable()
export class TravelDestinationsEffects {
    @Effect()
    newAdded$: Observable<Action> = this.actions$.pipe(
        ofType(TravelDestinationsActionTypes.NEW_DESTINATION),
        map((action: NewDestinationAction) => new FavoriteChosenAction(action.destination))
    );

    constructor(private actions$: Actions) {}
}
