import {
    reducerTravelDestinations,
    TravelDestinationsState,
    initializeTravelDestinationsState,
    InitMyDataAction,
    NewDestinationAction
    } from './travel-destinations-state.model';
import { TravelDestination } from './travel-destination.model';

describe('reducerTravelDestinations', () =>  {
    it('should reduce init data', () => {
        // setup
        const prevState: TravelDestinationsState = initializeTravelDestinationsState();
        const action: InitMyDataAction = new InitMyDataAction(['destination 1', 'destination 2']);
        // action
        const newState: TravelDestinationsState = reducerTravelDestinations(prevState, action);
        // assert
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].name).toEqual('destination 1');
        // tear down (empty)
    });

    it('should reduce new item added', () => {
        const prevState: TravelDestinationsState = initializeTravelDestinationsState();
        const action: NewDestinationAction = new NewDestinationAction(new TravelDestination('NYC', 'url'));
        const newState: TravelDestinationsState = reducerTravelDestinations(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].name).toEqual('NYC'); 
    });
});