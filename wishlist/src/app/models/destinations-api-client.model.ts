import { forwardRef, Inject, Injectable } from '@angular/core';
import { TravelDestination } from './travel-destination.model';
import { Store } from '@ngrx/store';
import {
		// TravelDestinationsState,
    NewDestinationAction,
		FavoriteChosenAction
	} from './travel-destinations-state.model';
import {AppConfig, AppState, APP_CONFIG, db} from './../app.module';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinationsApiClient {
  destinations:TravelDestination[]=[];
  constructor(private store: Store<AppState>,
  @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
  private http:HttpClient) {
		this.store
			.select(state => state.destinations)
			.subscribe((data) => {
				console.log("destinations sub store");
				console.log(data);
				this.destinations = data.items;
			});
		this.store
			.subscribe((data) => {
				console.log("all store");
				console.log(data);
			});
  }

  add(d: TravelDestination) {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-security'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', {new: d.name}, {headers: headers});
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NewDestinationAction(d));
        const myDb = db;
        myDb.destinations.add(d);
        console.log('all destinations in db!');
        myDb.destinations.toArray().then(destinations => console.log(destinations))
      }
    });
  }

  getById(id: String): TravelDestination {
    return this.destinations.filter(function(d) { return d.id.toString() === id; })[0];
  }

  getAll(): TravelDestination[] {
    return this.destinations;
  }

  chose(d: TravelDestination) {
    this.store.dispatch(new FavoriteChosenAction(d));
  }
}
