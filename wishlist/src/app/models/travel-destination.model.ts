import {v4 as uuid} from 'uuid';

export class TravelDestination {
	private selected: boolean;
  	public services: string[];
  	id = uuid();
  	//public votes = 0;

	constructor(public name: string, public url: string, public votes: number = 0) {
		this.services = ['breakfast', 'transportation', 'city tour', 'souvernirs'];
	}
	isSelected(): boolean {
		return this.selected;
	}
	setSelected(s: boolean) {
		this.selected = s;
	}
	voteUp(): any {
		this.votes++;
	}
	voteDown(): any {
		this.votes--;
	}
	reset(): any {
		this.votes=0;
	}
}
