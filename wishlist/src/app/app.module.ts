import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Dexie } from 'dexie';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TravelDestinationComponent } from './components/travel-destination/travel-destination.component';
import { DestinationListComponent } from './components/destination-list/destination-list.component';
import { DestinationDetailComponent } from './components/destination-detail/destination-detail.component';
import { TravelDestinationFormComponent } from './components/travel-destination-form/travel-destination-form.component';
import { TravelDestinationsState, reducerTravelDestinations, initializeTravelDestinationsState, TravelDestinationsEffects, InitMyDataAction } from './models/travel-destinations-state.model';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { LoggedUserGuard } from './guards/logged-user/logged-user.guard';
import { AuthService } from './services/auth.service';
import { FlightsComponent } from './components/flights/flights/flights.component';
import { FlightsMainComponent } from './components/flights/flights-main/flights-main.component';
import { FlightsMoreInfoComponent } from './components/flights/flights-more-info/flights-more-info.component';
import { FlightsDetailComponent } from './components/flights/flights-detail/flights-detail.component';
import { ReservationsModule } from './reservations/reservations.module';
import { TravelDestination } from './models/travel-destination.model';
import { SpymeDirective } from './spyme.directive';
import { ClickTrackingDirective } from './click-tracking.directive';



// init routing
export const childrenRoutesFlights: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: FlightsMainComponent },
  { path: 'more-info', component: FlightsMoreInfoComponent },
  { path: ':id', component: FlightsDetailComponent },
];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: DestinationListComponent },
  { path: 'destination/:id', component: DestinationDetailComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [ LoggedUserGuard ]
  },
  {
    path: 'flights',
    component: FlightsComponent,
    canActivate: [ LoggedUserGuard ],
    children: childrenRoutesFlights
  }
];
// end routing

// app config
export interface AppConfig {
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
// end app config

// app init
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeTravelDestinationsState();
}
@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http:HttpClient) {}
  async initializeTravelDestinationsState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-security'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', {headers:headers});
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
// end app init

// redux init
export interface AppState {
  destinations: TravelDestinationsState;
};

const reducers: ActionReducerMap<AppState> = {
  destinations: reducerTravelDestinations
};

const reducersInitialState = {
  destinations: initializeTravelDestinationsState()
};
// redux end init

// dexie db
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  destinations: Dexie.Table<TravelDestination, number>;
  translations: Dexie.Table<Translation, number>;
  constructor () {
    super('MyDatabase');
    this.version(1).stores({
      destinations: '++id, name, url',
    });
    this.version(2).stores({
      destinations: '++id, name, url',
      translations: '++id, lang, key, value',
    });
  }
}

export const db = new MyDatabase();
// end dexie db

// i18n init
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) {}

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
                    .where('lang')
                    .equals(lang)
                    .toArray()
                    .then(results => {
                                        if (results.length === 0) {
                                          return this.http
                                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                                            .toPromise()
                                            .then(apiResults => {
                                              db.translations.bulkAdd(apiResults);
                                              return apiResults;
                                            });
                                        }
                                        return results;
                                      }).then((traducciones) => {
                                        console.log('loaded translations: ');
                                        console.log(traducciones);
                                        return traducciones;
                                      }).then((traducciones) => {
                                        return traducciones.map((t) => ({[t.key]: t.value}));
                                      });
    return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}
// end i18n

@NgModule({
  declarations: [
    AppComponent,
    TravelDestinationComponent,
    DestinationListComponent,
    DestinationDetailComponent,
    TravelDestinationFormComponent,
    LoginComponent,
    ProtectedComponent,
    FlightsComponent,
    FlightsMainComponent,
    FlightsMoreInfoComponent,
    FlightsDetailComponent,
    SpymeDirective,
    ClickTrackingDirective,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1IjoiZWRnYXJlc3Bpbm9zYSIsImEiOiJja29zdDczczEwNDU4MnZrN2l3NXl1M20zIn0.dNQoJsjlPZWJ56td0pzlTw',
      geocoderAccessToken: 'pk.eyJ1IjoiZWRnYXJlc3Bpbm9zYSIsImEiOiJja29zdDczczEwNDU4MnZrN2l3NXl1M20zIn0.dNQoJsjlPZWJ56td0pzlTw'
    }),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    // AppRoutingModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, { 
      initialState: reducersInitialState,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
      } 
    }),
    EffectsModule.forRoot([TravelDestinationsEffects]),
    StoreDevtoolsModule.instrument(),
    ReservationsModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (HttpLoaderFactory),
          deps: [HttpClient]
      }
    })
  ],
  providers: [
    AuthService, LoggedUserGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },
    MyDatabase
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
