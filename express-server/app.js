var express = require("express"), cors = require("cors");
var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000"));

var cities = [ "Paris", "Madrid", "London", "NYC", "Berlin", "Rome" ];
// app.get("/cities", (req, res, next) => res.json(cities.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));
app.get("/cities", (req, res, next) => res.json(cities.filter((c) => c.toLowerCase().indexOf(String(req.query.q).toLowerCase()) > -1)));

var myDestinations = [];
app.get("/my", (req, res, next) => res.json(myDestinations));
app.post("/my", (req, res, next) => {
    console.log(req.body);
    myDestinations.push(req.body.new);
    res.json(myDestinations);
});

app.get("/api/translation", (req,res, next) => res.json([
    {lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang}
]));